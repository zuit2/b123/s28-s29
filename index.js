// load expressjs module
const express = require("express");

// create an application with expressjs and store it as app
const app = express();

// port is a variable to contain the port we want to designate
const port = 4000;

// express.json() allows us to handle the request's body and automatically parse the incoming JSON to a JS object we can access and manage.
app.use(express.json());

let users = [
    {
        email: "mighty12@gmail.com",
        username: "mightyMouse12",
        password: "notrelatedtomickey",
        isAdmin: false,
    },
    {
        email: "minnieMouse@gmail.com",
        username: "minniexmickey",
        password: "minniesincethestart",
        isAdmin: false,
    },
    {
        email: "mickeyTheMouse@gmail.com",
        username: "mickeyKing",
        password: "thefacethatrunstheplace",
        isAdmin: true,
    },
];

let items = [];

let loggedUser;

//Express has methods to use as routes corresponding to each HTTP method.
// get(<endpoint>,<functionToHandle request and responses>)
app.get("/", (req, res) => {
    //Once the route is accessed, we can send a response with the use of res.send()
    //res.send() actually combines writeHead() and end() already.
    // It is used to send a response to the client and is the end of the response.
    res.send("Hello World!");
});

app.get("/hello", (req, res) => {
    res.send("Hello from Batch 123!");
});

app.post("/", (req, res) => {
    // req.body will contain the body of a request.
    console.log(req.body);

    res.send(
        `Hello, I'm ${req.body.name}. I am ${req.body.age}. I could be described as ${req.body.description}.`
    );
    // res.send("Hello, I like Darkest Dungeon");
});

// create new user
app.post("/register", (req, res) => {
    console.log(req.body);

    let newUser = {
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        isAdmin: req.body.isAdmin,
    };

    users.push(newUser);
    console.log(users);

    res.send("Registered Successfully.");
});

// login
app.post("/users/login", (req, res) => {
    console.log("POST Login");
    // console.log(req.body);

    // object item
    let foundUser = users.find((user) => {
        return (
            user.username === req.body.username &&
            user.password === req.body.password
        );
    });

    if (foundUser !== undefined) {
        let foundUserIndex = users.findIndex((user) => {
            return user.username === foundUser.username;
        });

        // console.log(foundUser);
        // define index of foundUser
        foundUser.index = foundUserIndex;
        foundUser.password = null;
        // temporarily log user in. Allows to refer to the details of a logged in user.
        loggedUser = foundUser;
        console.log(loggedUser);

        res.send("Thank you for logging in.");
    } else {
        loggedUser = foundUser;
        res.send("Login failed. Wrong Credentials");
    }
});

/* ======= Activity ========

	addItems

		isAdmin: true 
		- additem 
		-console.log items
		-send message "You have added a new item."

		isAdmin: false 
		- send message "Unauthorized: Action Forbidden."

		items {name,description,price,isActive}
	
	GetAllItems

		isAdmin: true
		- send items array

		isAdmin: false
		- send message "Unauthorized: Action Forbidden"

*/

app.post("/items", (req, res) => {
    console.log("POST items");
    console.log(loggedUser);
    console.log(req.body);

    if (loggedUser !== undefined) {
        if (loggedUser.isAdmin === true) {
            let newItem = {
                name: req.body.name,
                description: req.body.description,
                price: req.body.price,
                isActive: req.body.isActive,
            };

            items.push(newItem);
            console.log(items);
            res.send("You have added a new item.");
        } else {
            res.send("Unauthorized: Action Forbidden.");
        }
    } else {
        res.send("Unauthorized: Action Forbidden.");
    }
});

// get all items
app.get("/items/getall", (req, res) => {
    console.log("GET All Items");
    console.log(loggedUser);

    if (loggedUser !== undefined) {
        if (loggedUser.isAdmin === true) {
            res.send(items);
        } else {
            res.send("Unauthorized: Action Forbidden.");
        }
    } else {
        res.send("Unauthorized: Action Forbidden.");
    }
});

// //addItem
// app.post('/items',(req,res)=>{

// 	console.log(loggedUser);
// 	console.log(req.body);

// 	if (loggedUser !== undefined){
// 		if (loggedUser.isAdmin === true) {

// 			// items {name,description,price,isActive}
// 			items.push(
// 				{
// 					name: req.body.name,
// 					description: req.body.description,
// 					price: req.body.price,
// 					isActive: req.body.isActive
// 				}
// 			)

// 			console.log(items);
// 			res.send('You have added a new item.');
// 		} else {
// 			res.send('Unauthorized: Action Forbidden');
// 		}
// 	} else {
// 			res.send('Unauthorized: Action Forbidden');
// 	}

// })

// //getAllItems
// app.get('/items/getall',(req,res)=>{

// 	console.log(loggedUser);

// 	if (loggedUser !== undefined){
// 		if (loggedUser.isAdmin === true) {
// 			console.log('getall isAdmin:true')
// 			console.log(items);
// 			res.send(items);
// 		}
// 		else {
// 		res.send('Unauthorized: Action Forbidden');
// 		}
// 	}
// 	else {
// 		res.send('Unauthorized: Action Forbidden');
// 	}

// })

//getSingleUser
// route parameters can be defined in the endpoint of a route with :parameterName
app.get("/users/:index", (req, res) => {
    // req.params is an object
    // its properties are then determined by your route parameters
    console.log(req.params);

    console.log(req.params.index);
    let index = parseInt(req.params.index);

    let user = users[index];
    res.send(user);
});

// updateUser
app.put("/users/:index", (req, res) => {
    console.log(req.params);
    console.log(req.params.index);
    let userIndex = parseInt(req.params.index);

    if (loggedUser !== undefined && loggedUser.index === userIndex) {
        // get the proper user from the array with our index
        users[userIndex].password = req.body.password;
        console.log(users[userIndex]);
        res.send("User password has been updated.");
    } else {
        res.send("Unauthorized. Login the correct user first.");
    }
});

// getSingleItem
app.get("/items/getSingle/:index", (req, res) => {
    console.log(req.params.index);
    let index = parseInt(req.params.index);
    let item = items[index];
    res.send(item);
});

// archiveItem
app.get("/items/archive/:index", (req, res) => {
    if (loggedUser !== undefined) {
        if (loggedUser.isAdmin === true) {
            console.log("archiveItem isAdmin:true");

            console.log(req.params.index);
            let index = parseInt(req.params.index);
            items[index].isActive = false;

            res.send("Item Archived.");
        } else {
            res.send("Unauthorized: Action Forbidden");
        }
    } else {
        res.send("Unauthorized: Action Forbidden");
    }
});

// activateItem
app.get("/items/activate/:index", (req, res) => {
    if (loggedUser !== undefined) {
        if (loggedUser.isAdmin === true) {
            console.log("activateItem isAdmin:true");

            console.log(req.params.index);
            let index = parseInt(req.params.index);
            items[index].isActive = true;

            res.send("Item Archived.");
        } else {
            res.send("Unauthorized: Action Forbidden");
        }
    } else {
        res.send("Unauthorized: Action Forbidden");
    }
});

app.listen(port, () => console.log(`Server is running at port ${port}`));
